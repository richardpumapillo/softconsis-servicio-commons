package com.softconsis.microservicio.app.commons.models.entity;

import java.io.Serializable;
import java.util.Date;


public class Empleado implements Serializable {

    private static final long serialVersionUID = 109361099674524889L;

    
    protected Long id;
    
	protected String nombre;
    
    protected String apellidoPaterno;
    
    protected String apellidoMaterno;
    
    protected String dni;
    
    protected String sexo;
    
    protected Direccion direccionCasa;
    
    protected Direccion direccionNacimiento;
   
    protected Date lastUpdated;

    protected Date createdOn;    
    
    protected Integer port;

    public Empleado() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
		this.id = id;
	}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Direccion getDireccionCasa() {
        return direccionCasa;
    }

    public void setDireccionCasa(Direccion direccionCasa) {
        this.direccionCasa = direccionCasa;
    }

    public Direccion getDireccionNacimiento() {
        return direccionNacimiento;
    }

    public void setDireccionNacimiento(Direccion direccionNacimiento) {
        this.direccionNacimiento = direccionNacimiento;
    }

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
	
	
    
    
}
